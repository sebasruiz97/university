<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NationalitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        
        DB::table('nationalities')->insert(['name' => 'Colombia']);

        DB::table('nationalities')->insert(['name' => 'United States']);

        DB::table('nationalities')->insert(['name' => 'Venezuela']);

        DB::table('nationalities')->insert(['name' => 'España']);

        DB::table('nationalities')->insert(['name' => 'Brasil']);

        DB::table('nationalities')->insert(['name' => 'México']);

        DB::table('nationalities')->insert(['name' => 'Argentina']);
    }
}
