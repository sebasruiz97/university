<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

class CampusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    DB::table('campus')->insert(['name' => 'Sede Principal']);

    DB::table('campus')->insert(['name' => 'Centro']);

    DB::table('campus')->insert(['name' => 'Sur']);

    DB::table('campus')->insert(['name' => 'Oriente']);
    }
}
