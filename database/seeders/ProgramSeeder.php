<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

class ProgramSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('programs')->insert(['name' => 'Ingeniería de Sistemas', 'faculty_id' => 1, 'photo'=> '0']);

        DB::table('programs')->insert(['name' => 'Artes Plásticas', 'faculty_id' => 2 , 'photo'=> '0' ] );
    
        DB::table('programs')->insert(['name' => 'Licenciatura en Matemáticas', 'faculty_id' => 3, 'photo'=> '0']);
    
        DB::table('programs')->insert(['name' => 'Economía', 'faculty_id' => 4, 'photo'=> '0']);
    }
}
