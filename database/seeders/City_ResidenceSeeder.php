<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

class City_ResidenceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cities_residence')->insert(['name' => 'Bogotá D.C.', 'nationality_id' => 1]);

    DB::table('cities_residence')->insert(['name' => 'Cali', 'nationality_id' => 1]);

    DB::table('cities_residence')->insert(['name' => 'Medellín', 'nationality_id' => 1]);

    DB::table('cities_residence')->insert(['name' => 'Barranquilla', 'nationality_id' => 1]);

    DB::table('cities_residence')->insert(['name' => 'New York', 'nationality_id' => 2]);

    DB::table('cities_residence')->insert(['name' => 'Caracás', 'nationality_id' => 3]);

    DB::table('cities_residence')->insert(['name' => 'Madrid', 'nationality_id' => 4]);

    DB::table('cities_residence')->insert(['name' => 'Barcelona', 'nationality_id' => 4]);

    DB::table('cities_residence')->insert(['name' => 'Sao Paulo', 'nationality_id' => 5]);

    DB::table('cities_residence')->insert(['name' => 'México D.F.', 'nationality_id' => 6]);

    DB::table('cities_residence')->insert(['name' => 'Buenos Aires', 'nationality_id' => 7]);
    }
}
