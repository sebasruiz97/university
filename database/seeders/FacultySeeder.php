<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

class FacultySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('faculties')->insert(['name' => 'Ingeniería', 'campus_id' => 1]);

    DB::table('faculties')->insert(['name' => 'Artes', 'campus_id' => 2]);

    DB::table('faculties')->insert(['name' => 'Licenciatura', 'campus_id' => 3]);

    DB::table('faculties')->insert(['name' => 'Ciencias Económicas', 'campus_id' => 4]);
    }
}
