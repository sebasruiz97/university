<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Database\Seeders\NationalitySeeder;
use Database\Seeders\City_OriginSeeder;
use Database\Seeders\City_ResidenceSeeder;
use Database\Seeders\CampusSeeder;
use Database\Seeders\FacultySeeder;
use Database\Seeders\ProgramSeeder;
use Database\Seeders\RoleSeeder;
use Database\Seeders\StudentSeeder;
use Database\Seeders\AdministratorSeeder;

use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

       

       $this->call(NationalitySeeder::class);
       $this->call(City_OriginSeeder::class);
       $this->call(City_ResidenceSeeder::class);
       $this->call(CampusSeeder::class);
       $this->call(FacultySeeder::class);
       $this->call(ProgramSeeder::class);
       $this->call(RoleSeeder::class);
       $this->call(StudentSeeder::class);
       $this->call(AdministratorSeeder::class);


    }
}
