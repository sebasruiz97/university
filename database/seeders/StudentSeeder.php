<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;


use Illuminate\Support\Facades\DB;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('students')->insert(['names' => 'Juan Felipe',
        'surnames' => 'Sanchez Correa', 'student_code' => 'JAFE05654',
        'address' => 'Calle 40 # 5-84', 'phone_number' => 3189456782,
        'email' => 'jasanchez@gmail.com', 'password' => '12345678',
        'city_residence_id' => 4,  'city_origin_id' => 1 , 'program_id' => 1,
        'role_id' =>  2
        ]);
    }
}
