<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->id();
            $table->string('names');
            $table->string('surnames');
            $table->string('student_code');
            $table->string('address');
            $table->bigInteger('phone_number')->unsigned();
            $table->string('email');
            $table->string('password');
            $table->bigInteger('city_residence_id')->unsigned();
            $table->bigInteger('city_origin_id')->unsigned();
            $table->bigInteger('program_id')->unsigned();
            $table->bigInteger('role_id')->unsigned();
            $table->foreign('city_residence_id')->references('id')->on('cities_residence');
            $table->foreign('city_origin_id')->references('id')->on('cities_origin');
            $table->foreign('program_id')->references('id')->on('programs');
            $table->foreign('role_id')->references('id')->on('roles');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
