<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProgramRequest;
use App\Models\Faculty;
use App\Models\Program;
use Dotenv\Validator;
use Illuminate\Http\Client\Request as ClientRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator as FacadesValidator;
use Illuminate\Validation\Validator as ValidationValidator;
use Illuminate\Support\Facades\Storage;






class ProgramController extends Controller
{
    

    public function index(){

        $programs = Program::all();

        return view('program.index', compact('programs'));


    }

    public function create(){

        $faculties = Faculty::all();

        return view('program.create', compact('faculties'));
    }

    public function store(Request $request){


        $fields = [
            'name' => 'required',
            'faculty_id' => 'required',
            'photo' => 'required'
        ];

        $message = ['required' => 'El campo :attribute es obligatorio y requerido'];


        $this->validate($request,$fields,$message);





     $dates =  $request->except('_token');

      if($request->hasFile('photo')){

      $dates['photo'] = $request->file('photo')->store('img','public');

      }  

        
       
        Program::insert($dates);

        return redirect()->route('program.index');







    }

    public function show($id){

$programs = Program::find($id)->where('id','=',$id)->get();

return view('program.show', compact('programs'));

    }


    public function edit($id){




        $programs = Program::find($id)->where('id','=',$id)->get();

        $faculties = Faculty::all();

        return view('program.edit', compact('programs','faculties'));
        
    }

    public function update(Request $request,$id){

        $dates = $request->except(['_token','_method']);

        if($request->hasFile('photo')){

            $program = Program::findOrFail($id);

            Storage::delete('public/'.$program->photo);

            $dates['photo'] = $request->file('photo')->store('img','public');
      
            }  


        Program::find($id)->where('id','=', $id)->update($dates);

        $program = Program::findOrFail($id);
       

        return redirect()->route('program.show',$id);


    }


    public function destroy($id){

        Program::find($id)->delete();

        return redirect()->route('program.index');
    }
}
