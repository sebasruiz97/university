<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Administrator;

Use App\Models\Student;

Use App\Models\Role;

use App\Models\City_Origin;

Use App\Models\City_Residence;

use App\Models\Program;

class AdministratorController extends Controller
{
    
    public function index() {

        $students = Student::all();

        return view('administrator.index', compact('students'));

    }

    public function create(){

     $cities_origin = City_Origin::all();

     $cities_residence = City_Residence::all();

     $roles = Role::all();

     $programs = Program::all();


        return view('administrator.create')->
        with(compact('cities_origin','cities_residence'))->
        with(compact('roles', 'programs'));
    }


    public function store(Request $request){

        $fields = [
            'names' => 'required',
            'surnames' => 'required'  ,
            'student_code' => 'required',
            'address' => 'required',
            'phone_number' => 'required',
            'email' => 'required',
            'password' => 'required',
            'city_residence_id' => 'required',
            'city_origin_id' => 'required',
            'program_id' => 'required',
            'role_id' => 'required'
        ];

        $message = ['required' => 'El campo :attribute es obligatorio y requerido'];


        $this->validate($request,$fields,$message);

      Student::create($request->all());

      return redirect()->route('administrator.index');

    }

    public function show($id){

    $student_query = Student::find($id)->all()->where('id', '=', $id);
    
     return view('administrator.show',compact('student_query'));


    }

    public function edit($id){

        $student_query = Student::find($id)->all()->where('id', '=', $id);

        $cities_residence = City_Residence::all();

        $cities_origin = City_Origin::all();

        $programs = Program::all();

        $roles = Role::all();

        return view('administrator.edit')->with(compact('student_query', 'cities_residence'))
        ->with(compact('cities_origin','programs'))->with(compact('roles'));
    }

    public function update(Request $request, $id){

        Student::find($id)->update($request-> all());

        return redirect()->route('administrator.show', $id);
    }

    public function destroy($id){

        Student::find($id)->delete();

        return redirect()->route('administrator.index');
    }


    
}
