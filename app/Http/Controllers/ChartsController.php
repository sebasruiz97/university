<?php

namespace App\Http\Controllers;

use App\Models\Program;
use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;







class ChartsController extends Controller
{
    
    public function index(){

        $students = Student::selectRaw('program_id, count(*) as total')->groupBy('program_id')->get();

        return view('administrator.student', compact('students'));
    }
}
