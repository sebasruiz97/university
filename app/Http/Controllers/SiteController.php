<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Program;

class SiteController extends Controller
{
    

    public function index(){

        $programs = Program::all();

        return view('site.index', compact('programs'));


    }



}
