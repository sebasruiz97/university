<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Student;

use App\Models\Administrator;

class Role extends Model
{
    use HasFactory;

    protected $primaryKey = 'id';

    protected $table = 'roles';
    
    protected $fillable = [
    'name'
    ];

    public function students(){

        return $this->hasMany(Student::class,'id');
    }
    
    public function administrators(){

        return $this->hasMany(Administrator::class,'id');
    }

}

