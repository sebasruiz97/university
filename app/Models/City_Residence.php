<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Nationality;
use App\Models\Student;

class City_Residence extends Model

{
    use HasFactory;

    protected $primaryKey = 'id';

    protected $table = 'cities_residence';
    
    protected $fillable = [
    'name','nationality_id'
    ];

    public function nationality(){

        return $this->belongsTo(Nationality::class,'nationality_id');
    }

    public function students(){

        return $this->hasMany(Student::class,'id');
    }
}

