<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\City_Residence;
use App\Models\City_Origin;
use App\Models\Program;
use App\Models\Role;

class Student extends Model
{
    use HasFactory;

    protected $primaryKey = 'id';

    protected $table = 'students';
    
    protected $fillable = [
    'names','surnames','student_code','address','phone_number',
    'email','password','city_residence_id','city_origin_id',
    'program_id','role_id'
    ];


    public function city_residence(){

        return $this->belongsTo(City_Residence::class,'city_residence_id');
    }

    public function city_origin(){

       return $this->belongsTo(City_Origin::class,'city_origin_id');


    }

    public function program(){

        return $this->belongsTo(Program::class,'program_id');
    }


    public function role(){

        return $this->belongsTo(Role::class,'role_id');
    }


}
