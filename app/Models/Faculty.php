<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Campus;

use App\Models\Program;

class Faculty extends Model
{
    use HasFactory;

    protected $primaryKey = 'id';

    protected $table = 'faculties';
    
    protected $fillable = [
    'name','campus_id'
    ];

    public function campus(){

        return $this->belongsTo(Campus::class,'campus_id');
    }

    public function programs(){

        return $this->hasMany(Program::class,'id');
    }
    
}
