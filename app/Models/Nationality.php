<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\City_Origin;

use App\Models\City_Residence;

class Nationality extends Model
{
    use HasFactory;

    protected $primaryKey = 'id';

    protected $table = 'nationalities';
    
    protected $fillable = [
    'name'
    ];

    public function cities_origin(){

        return $this->hasMany(City_Origin::class,'id');

    }

    public function cities_residence(){

        return $this->hasMany(City_Residence::class,'id');

    }
}
