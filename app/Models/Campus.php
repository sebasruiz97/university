<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Faculty;

class Campus extends Model
{
    use HasFactory;

    protected $primaryKey = 'id';

    protected $table = 'campus';
    
    protected $fillable = [
    'name'
    ];

    public function faculties(){

        return $this->hasMany(Faculty::class,'id');
    }
}
