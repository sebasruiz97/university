<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Faculty;

use App\Models\Student;

class Program extends Model
{
    use HasFactory;

    protected $primaryKey = 'id';

    protected $table = 'programs';
    
    protected $fillable = [
    'name','faculty_id','photo'
    ];

    public function faculty() {

        return $this->belongsTo(Faculty::class,'faculty_id');
    }

    public function students(){

        return $this->hasMany(Student::class,'id');
    }
}

