<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Role;

class Administrator extends Model
{
    use HasFactory;

    protected $primaryKey = 'id';

    protected $table = 'administrators';
    
    protected $fillable = [
    'name','email','password','role_id'
    ];

    public function role(){

        return $this->belongsTo(Role::class,'role_id');

    }


}
