<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AdministratorController;

use App\Http\Controllers\LoginController;

use App\Http\Controllers\ChartsController;

use App\Http\Controllers\ProgramController;

use App\Http\Controllers\SiteController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Controladores del Administrador

Route::get('/administrator',[AdministratorController::class, 'index'])->name('administrator.index');
Route::get('/administrator/create',[AdministratorController::class,'create'])->name('administrator.create');
Route::post('/administrator',[AdministratorController::class,'store'])->name('administrator.store');
Route::get('/administrator/{id}',[AdministratorController::class,'show'])->name('administrator.show');
Route::get('/administrator/edit/{id}',[AdministratorController::class,'edit'])->name('administrator.edit');
Route::put('/administrator/{id}',[AdministratorController::class,'update'])->name('administrator.update');
Route::delete('/administrator/{id}',[AdministratorController::class,'destroy'])->name('administrator.destroy');


// Google Charts

Route::get('charts',[ChartsController::class,'index'])->name('charts.index');

//Controladores de Login
Route::get('/login',[LoginController::class,'index'])->name('login.index');
Route::post('/login/verify',[LoginController::class,'verify'])->name('login.verify');



// Programa
Route::get('program',[ProgramController::class,'index'])->name('program.index');
Route::get('program/create',[ProgramController::class,'create'])->name('program.create');
Route::post('program',[ProgramController::class,'store'])->name('program.store');
Route::delete('program/{id}',[ProgramController::class,'destroy'])->name('program.destroy');
Route::get('program/{id}',[ProgramController::class,'show'])->name('program.show');
Route::get('program/edit/{id}',[ProgramController::class,'edit'])->name('program.edit');
Route::put('program/{id}',[ProgramController::class,'update'])->name('program.update');


// Micrositio
Route::get('site',[SiteController::class,'index'])->name('site.index');