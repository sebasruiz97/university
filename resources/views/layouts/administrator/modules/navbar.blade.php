<nav class="navbar has-background-link" role="navigation" aria-label="main navigation">



 <!--  Ciudades -->


    <div class="columns">

     <div class="column is-2  ">

      <div class="navbar-item has-dropdown is-hoverable   ">

      <a class="navbar-link has-text-white ">
      <i class="fas fa-user-graduate"></i>
          Estudiantes
        </a>

        <div class="navbar-dropdown has-background-link">
          <a class="navbar-item has-text-white "  href="{{route('administrator.index')}}">
            Mostrar Estudiantes
          </a>
          <a class="navbar-item has-text-white " href="{{route('administrator.create')}}">
            Crear Estudiante
          </a>
        </div>
      </div>

      </div>

  <!-- Propietarios  -->

  <div class="column is-2  ">
      <div class="navbar-item has-dropdown is-hoverable ">
        <a class="navbar-link has-text-white ">
        <i class="fas fa-file-alt"></i>
         Programas
        </a>

        <div class="navbar-dropdown has-background-link">
          <a class="navbar-item has-text-white " href="{{route('program.index')}}">
            Mostrar Programas
          </a>
          <a class="navbar-item has-text-white " href="{{route('program.create')}}">
           Crear Programa
          </a>
        </div>
      </div>
      </div>



  <!--  Conductores  -->

       <div class="column is-2">
      <div class="navbar-item has-dropdown is-hoverable" >
        <a class="navbar-link has-text-white ">
        <i class="fas fa-chart-pie"></i>
          Gráficos
        </a>

        <div class="navbar-dropdown has-background-link">
          <a class="navbar-item has-text-white " href="{{route('charts.index')}}">
            Mostrar Cantidad de Estudiantes por programa
          </a>
          <a class="navbar-item has-text-white " href=#>
            --
          </a>
        </div>
      </div>
      </div>


  <!--  Colores -->
    <div class="column is-2">
      <div class="navbar-item has-dropdown is-hoverable">
        <a class="navbar-link has-text-white">
        <i class="fas fa-palette"></i>
          --
        </a>

        <div class="navbar-dropdown has-background-link">
          <a class="navbar-item has-text-white " href=#>

            --
          </a>
          <a class="navbar-item has-text-white " href=#>
            --
          </a>
        </div>
      </div>
      </div>


  <!--  Marcas -->
    <div class="column is-1">
      <div class="navbar-item has-dropdown is-hoverable has-background-link">
        <a class="navbar-link has-text-white ">
        <i class="fas fa-car-side"></i>
         --
        </a>

        <div class="navbar-dropdown has-background-link">
          <a class="navbar-item has-text-white" href=#>
            --
          </a>
          <a class="navbar-item has-text-white" href=#>
         --
          </a>
        </div>
      </div>
      </div>




  <!--  Tipo de Vehículos -->
    <div class="column is-1">
      <div class="navbar-item has-dropdown is-hoverable has-background-link">
        <a class="navbar-link has-text-white " >
        <i class="fas fa-caravan"></i>
          --
        </a>

        <div class="navbar-dropdown has-background-link">
          <a class="navbar-item has-text-white " href=#>
            --
          </a>
          <a class="navbar-item has-text-white" href=#>
           --
          </a>
        </div>
      </div>
      </div>


  <!--  Vehículos -->
    <div class="column is-2">
      <div class="navbar-item has-dropdown is-hoverable has-background-link">
        <a class="navbar-link has-text-white " >
        <i class="fas fa-car"></i>
        --
        </a>

        <div class="navbar-dropdown has-background-link">
          <a class="navbar-item has-text-white" href=#>
           --
          </a>
          <a class="navbar-item has-text-white " href=#>
           --
          </a>
        </div>
      </div>


      </div>








      </div>

</nav>

