<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Administrador de Universidad</title>
   <link rel="stylesheet" href="{{url('css/app.css')}}" media="" > 
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">
  
</head>
<body>

<!--  NavBar  -->

@include('layouts.administrator.modules.navbar')

<!--   Información de la Universidad -->

<h1 class="is-size-2 has-text-centered has-text-dark"> Bienvenido a la Administración de la Universidad</h1>




<!-- SideBar-->
@include('layouts.administrator.modules.sidebar')

<!-- Page Content  -->
@yield('content')

<!-- Footer -->
@include('layouts.administrator.modules.footer')



    
</body>
</html>