@extends('layouts.administrator.app')

@section('content')

<h2 class="has-text-info has-text-centered is-size-3 mb-4 mt-4"> Registrar Estudiante</h2>

@if(count($errors)>0)
<div class="notification is-danger ">
  <button class="delete"></button>
  <ul>
  @foreach($errors->all() as $error)
<li>{{$error}}</li>
  @endforeach
  </ul>
 </div>
 @endif


<form class='mb-4' action="{{route('administrator.store')}}" method="post">

@csrf


  <div class="form-control my-4 ">
<label class=" is-size-4 my-4" for="names"> Escriba sus Nombres: </label>
<input class="ml-3 my-4 input {{$errors->has('names')?'is-danger':''}}"   type="text" name="names" placeholder="Escriba sus Nombres:">
{!! $errors->first('names', '<p class="help is-danger">:message</p>') !!}
  </div>




  <div class="form-control my-4">
<label for="surnames" class=" is-size-4 my-4"> Escriba sus Apellidos: </label>
<input  class="ml-3 my-4 input {{$errors->has('surnames')?'is-danger':''}} " type="text" name="surnames" placeholder="Escriba sus Apellidos:" >
{!! $errors->first('surnames', '<p class="help is-danger">:message</p>') !!}
</div>



  <div class="form-control my-4">
<label for="student_code" class=" is-size-4 my-4"> Escriba el Código del Estudiante: </label>
<input  class="ml-3 my-4 input {{$errors->has('student_code')?'is-danger':''}} "  type="text" name="student_code" placeholder="Escriba el Código del Estudiante:">
{!! $errors->first('student_code', '<p class="help is-danger">:message</p>') !!}
</div>


  <div class="form-control my-4">
<label for="address" class=" is-size-4"> Escriba la Dirección del Estudiante: </label>
<input  class="ml-3 my-4 input {{$errors->has('address')?'is-danger':''}} " type="text" name="address" placeholder="Escriba la dirección del Estudiante:">
{!! $errors->first('address', '<p class="help is-danger">:message</p>') !!}
</div>



  <div class="form-control my-4">
<label for="phone_number" class=" is-size-4 my-4"> Escriba el Teléfono del Estudiante: </label>
<input  class="ml-3 my-4 input {{$errors->has('phone_number')?'is-danger':''}}" type="number" name="phone_number" placeholder="Escriba el Teléfono del Estudiante:">
{!! $errors->first('phone_number', '<p class="help is-danger">:message</p>') !!}
</div>



  <div class="form-control my-4">
<label for="email" class=" is-size-4 my-4"> Escriba el Correo del Estudiante: </label>
<input  class="ml-3 my-4 input {{$errors->has('email')?'is-danger':''}} " type="email" name="email" placeholder="Escriba el Correo del Estudiante:">
{!! $errors->first('email', '<p class="help is-danger">:message</p>') !!}
</div>


  <div class="form-control my-4">
<label for="password" class=" is-size-4 my-4"> Escriba la Contraseña del Estudiante: </label>
<input  class="ml-3 my-4 input {{$errors->has('password')?'is-danger':''}} " type="password" name="password" placeholder="Escriba el Contraseña del Estudiante:">
{!! $errors->first('password', '<p class="help is-danger">:message</p>') !!}
</div>



  <div class="form-control my-4">
<label for="city_residence_id" class=" is-size-4 my-4"> Selecciona la Ciudad de Residencia: </label>
<select class="select ml-3 my-4 input  {{$errors->has('city_residence_id')?'is-danger':''}}" name="city_residence_id" >
@foreach($cities_residence as $city_residence)
<option  value="{{$city_residence->id}}">{{$city_residence->name}}</option>
@endforeach
</select>
{!! $errors->first('city_residence_id', '<p class="help is-danger">:message</p>') !!}
</div>




  <div class="form-control my-4">
<label for="city_origin_id" class=" is-size-4 my-4 "> Selecciona la Ciudad de Origen: </label>
<select class="select ml-3 my-4 input {{$errors->has('city_origin_id')?'is-danger':''}} " name="city_origin_id" >
@foreach($cities_origin as $city_origin)
<option value="{{$city_origin->id}}">{{$city_origin->name}}</option>
@endforeach
</select>
{!! $errors->first('city_origin_id', '<p class="help is-danger">:message</p>') !!}
</div>




  <div class="form-control my-4">
<label for="program_id" class=" is-size-4 my-4"> Selecciona el Programa: </label>
<select class="select ml-3 my-4 input {{$errors->has('program_id')?'is-danger':''}} " name="program_id" >
@foreach($programs as $program)
<option value="{{$program->id}}">{{$program->name}}</option>
@endforeach
</select>
{!! $errors->first('program_id', '<p class="help is-danger">:message</p>') !!}
</div>



  <div class="form-control my-4">
<label for="role_id" class=" is-size-4 my-4 {{$errors->has('role_id')?'is-danger':''}}"> Selecciona el Rol: </label>
<select class="select my-4  ml-3 input " name="role_id" >
@foreach($roles as $role)
<option value="{{$role->id}}">{{$role->name}}</option>
@endforeach
</select>
{!! $errors->first('role_id', '<p class="help is-danger">:message</p>') !!}
</div>



<input type="submit" class="button has-background-success has-text-white" value="Registrar Estudiante">




</form>





@endsection



