@extends('layouts.administrator.app')
    @section('content')
    <html>
  <head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Programa', 'Estudiantes'],
          @foreach($students as $student)

          ['{{$student->program->name}}', {{$student->total}}, ],

          @endforeach
        ]);


        var options = {
          title: 'Gráfica de la Cantidad de Estudiantes en un Programa',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
        chart.draw(data, options);
      }
    </script>
  </head>
  <body>
    <div id="piechart_3d" style="width: 900px; height: 500px;"></div>
  </body>
</html>



        
    @endsection







