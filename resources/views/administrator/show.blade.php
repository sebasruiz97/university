@extends('layouts.administrator.app')


@section('content')

<h2 class="is-size-2 has-text-centered mb-4"> Datos del Estudiante</h2>

<table class="table is-bordered has-text-centered" >

<thead>
<tr class="has-background-link is-size-5">
<td class="has-text-white ">Nombres del Estudiante</td>
<td class="has-text-white">Apellidos del Estudiante</td>
<td class="has-text-white">Código del Estudiante</td>
<td class="has-text-white">Dirección</td>
<td class="has-text-white">Teléfono</td>
<td class="has-text-white">Correo</td>
<td class="has-text-white">Contraseña</td>
<td class="has-text-white">Nacionalidad</td>
<td class="has-text-white">Ciudad de Residencia</td>
<td class="has-text-white">Ciudad de Origen</td>
<td class="has-text-white">Programa</td>
<td class="has-text-white">Facultad</td>
<td class="has-text-white" >Campus</td>
<td class="has-text-white">Rol</td>
</tr>
</thead>

<tbody>
@foreach($student_query as $student)
<tr>
<td>{{$student->names}}</td>
<td>{{$student->surnames}}</td>
<td>{{$student->student_code}}</td>
<td>{{$student->address}}</td>
<td>{{$student->phone_number}}</td>
<td>{{$student->email}}</td>
<td>{{$student->password}}</td>
<td>{{$student->city_origin->nationality->name}}</td>
<td>{{$student->city_residence->name}}</td>
<td>{{$student->city_origin->name}}</td>
<td>{{$student->program->name}}</td>
<td>{{$student->program->faculty->name}}</td>
<td>{{$student->program->faculty->campus->name}}</td>
<td>{{$student->role->name}}</td>


</tr>
@endforeach
</tbody>



</table>


<a class="button has-background-success has-text-white  mb-5" href="{{route('administrator.index')}}">Volver Mostrar Estudiantes</a>

@endsection