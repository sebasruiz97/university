@extends('layouts.administrator.app')
    @section('content')
        <div class="columns">

            <div class="column is-full">
                <h2 class="is-size-3 has-text-dark has-text-centered"> Estudiantes </h2>
            </div>
        </div>

        <div class="columns">

            <div class="column is-full" align="center">
                <a class="button has-background-info has-text-white" href="{{route('administrator.create')}}">Registrar Estudiante</a>

                <a class="button has-background-info has-text-white" href="{{route('charts.index')}}">Mostrar Cantidad de Estudiantes por Programa</a>

            </div>

        </div>

        <div class="columns">

            <table class="table  is-bordered has-text-centered column is-full  ">
            

                <tr class="has-text-weight-semibold is-size-6 mt-4 has-background-link has-text-white " >
<td>Nombres</td>
<td>Apellidos</td>
<td>Código del Estudiante</td>
<td>Dirección</td>
<td>Teléfono</td>
<td>Correo</td>
<td>Nacionalidad</td>
<td>Ciudad de Residencia</td>
<td>Ciudad de Origen</td>
<td>Campus</td>
<td>Facultad</td>
<td>Programa</td>
<td>Rol</td>
<td>Acciones</td>
                </tr>

                <tr>
                @foreach($students as $student)
<tr class="has-background-white has-text-dark is-size-6">
<td>{{$student->names}}</td>
<td>{{$student->surnames}}</td>
<td>{{$student->student_code}}</td>
<td>{{$student->address}}</td>
<td>{{$student->phone_number}}</td>
<td>{{$student->email}}</td>
<td>{{$student->city_origin->nationality->name}}</td>
<td>{{$student->city_residence->name}}</td>
<td>{{$student->city_origin->name}}</td>
<td>{{$student->program->faculty->campus->name}}</td>
<td>{{$student->program->faculty->name}}</td>
<td>{{$student->program->name}}</td>
<td>{{$student->role->name}}</td>

                        <td>


                            <form action="{{route('administrator.destroy',$student->id)}}" method="post">

                                @csrf

                                @method('DELETE')

                                <a href="{{route('administrator.edit',$student->id)}}" class="button is-info">Actualizar </a>
                                <a href="{{route('administrator.show',$student->id)}}" class="button is-warning">Mostrar </a>
                      <button class="button is-danger">Eliminar</button>
                            </form>
                        </td>
                </tr>

                @endforeach






            </table>

        </div>
    @endsection







