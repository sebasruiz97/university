@extends('layouts.administrator.app')

@section('content')

<h2 class="has-text-centered is-size-2"> Actualizar Datos del Estudiante</h2>

@foreach($student_query as $student)
<form action="{{route('administrator.update',$student->id)}}" method="post">
@csrf
@method('PUT')


<label class="label is-size-4" for="names">Nombres:</label>
<input class="input" type="text" name="names" value="{{$student->names}}" >

<label  class="label  is-size-4" for="surnames">Apellidos:</label>
<input  class="input" type="text" name="surnames" value="{{$student->surnames}}" >

<label  class="label  is-size-4" for="student_code">Código del Estudiante:</label>
<input  class="input" type="text" name="student_code" value="{{$student->student_code}}" >

<label  class="label  is-size-4" for="address">Dirección:</label>
<input  class="input" type="text" name="address" value="{{$student->address}}" >

<label  class="label  is-size-4" for="email">Correo:</label>
<input  class="input" type="text" name="email" value="{{$student->email}}" >

<label  class="label" for="password">Contraseña:</label>
<input  class="input" type="password" name="password" value="{{$student->password}}" >

<label  class="label  is-size-4" for="city_residence_id">Ciudad de Residencia:</label>
<select  class="input" name="city_residence_id" >
<option value="{{$student->city_residence_id}}">{{$student->city_residence->name}}</option>
@foreach($cities_residence as $city_residence)
<option value="{{$city_residence->id}}">{{$city_residence->name}}</option>
@endforeach
</select>

<label  class="label  is-size-4" for="city_origin_id">Ciudad de Origen:</label>
<select  class="input" name="city_origin_id" >
<option value="{{$student->city_origin_id}}">{{$student->city_origin->name}}</option>
@foreach($cities_origin as $city_origin)
<option value="{{$city_origin->id}}">{{$city_origin->name}}</option>
@endforeach
</select>

<label  class="label  is-size-4" for="program_id">Programa:</label>
<select  class="input" name="program_id" >
<option value="{{$student->program_id}}">{{$student->program->name}}</option>
@foreach($programs as $program)
<option value="{{$program->id}}">{{$program->name}}</option>
@endforeach
</select>

<label  class="label  is-size-4" for="role_id">Rol:</label>
<select  class="input" name="role_id" >
<option value="{{$student->role_id}}">{{$student->role->name}}</option>
@foreach($roles as $role)
<option value="{{$role->id}}">{{$role->name}}</option>
@endforeach
</select>

<input class="button has-background-success has-text-white my-4 mt-4" type="submit" value="Actualizar Datos del Estudiante">

</form>


@endforeach
@endsection


