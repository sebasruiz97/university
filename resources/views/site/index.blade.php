<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Universidad- Sitio Oficial</title>
    <link rel="stylesheet" href="{{url('css/app.css')}}" media="" > 
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">
</head>
<body >

<h1 class="title is-size-2 has-text-link has-text-centered mx-4 my-4"> Bienvenido al Sitio Oficial de la Universidad </h1>


<h2 class="title is-size-3 has-text-centered has-text-link my-4 mx-4"> Programas de la Universidad</h2>


<a class="button has-background-link has-text-white mx-4 my-4" href="{{route('login.index')}}">Iniciar Sesión </a>

@foreach($programs as $program)





<div class=" card mx-4 my-4 ">

<div class="columns media-content"> 

<div class="column is-3">
<img class="" src="{{asset('storage').'/'.$program->photo}}" width="200" alt="Foto">
</div>

<div class="column is-3">
<p class="title is-size-4 has-text-centered"> Programa: {{$program->name}}</p>
</div>

<div class="column is-3">
<p class="subtitle is-size-5 has-text-centered  ">Campus: {{$program->faculty->campus->name}}</p>
</div>

<div class="column is-3">
<p class="subtitle is-size-5 has-text-centered  ">Facultad: {{$program->faculty->name}}</p>
</div>


</div>
</div>






@endforeach
</body>
</html>


