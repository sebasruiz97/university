<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <title>Iniciar Sesión</title>
    <link rel="stylesheet" href="{{url('css/app.css')}}">

  
</head>
<body class="has-background-info">
    
    <form method="post" action="{{route('login.verify')}}">

@csrf
    <h2 class="has-text-centered is-size-2 mx-4 my-4 has-text-white"> Iniciar Sesión en la Universidad</h2>
    
    <div class="form-control mx-4 my-4">
    <label class="label is-size-4" for="email">Correo: </label>
    <input class="input" type="text" name="email">
    </div>


<div class="form-control mx-4 my-4">
    <label class="label  is-size-4" for="password">Contraseña: </label>
    <input class="input" type="password" name="password">
    </div>
  
    
    <input class="button has-background-success my-4 mx-4" type="submit" value="Iniciar Sesión">
    
    </form>

    



    
</body>
</html>