@extends('layouts.administrator.app')
    @section('content')
        <div class="columns">

            <div class="column is-full">
                <h2 class="is-size-3 has-text-dark has-text-centered"> Estudiantes </h2>
            </div>
        </div>

        <div class="columns">

            <div class="column is-full" align="center">
                <a class="button has-background-info has-text-white" href="{{route('program.create')}}">Registrar Programa</a>

              

            </div>

        </div>

        <div class="columns">

            <table class="table  is-bordered has-text-centered column is-full  ">
            

                <tr class="has-text-weight-semibold is-size-6 mt-4 has-background-link has-text-white " >
<td>Nombre</td>
<td>Facultad</td>
<td>Campus</td>
<td>Foto</td>
<td>Acciones</td>

                </tr>

                <tr>
                @foreach($programs as $program)
<tr class="has-background-white has-text-dark is-size-6">

<td>{{$program->name}}</td>
<td>{{$program->faculty->name}}</td>
<td>{{$program->faculty->campus->name}}</td>
<td><img    src="{{asset('storage').'/'. $program->photo}}" alt="Foto" width="200px" height="100px"> </td>


                        <td>


                            <form action="{{route('program.destroy',$program->id)}}" method="post">

                                @csrf

                                @method('DELETE')

                                <a href="{{route('program.edit',$program->id)}}" class="button is-info">Actualizar </a>
                                <a href="{{route('program.show',$program->id)}}" class="button is-warning">Mostrar </a>
                      <button class="button is-danger">Eliminar</button>
                            </form>
                        </td>
                </tr>

                @endforeach






            </table>

        </div>
    @endsection







