@extends('layouts.administrator.app')

@section('content')

<h2 class="has-text-centered is-size-2"> Actualizar Datos del Estudiante</h2>

@foreach($programs as $program)
<form action="{{route('program.update',$program->id)}}" method="post" enctype="multipart/form-data">
@csrf
@method('PUT')


<label class="label is-size-4" for="names">Programa:</label>
<input class="input" type="text" name="name" value="{{$program->name}}" >

<label  class="label  is-size-4" for="city_residence_id">Facultad:</label>
<select  class="input" name="faculty_id" >
<option value="{{$program->faculty_id}}">{{$program->faculty->name}}</option>
@foreach($faculties as $faculty)
<option value="{{$faculty->id}}">{{$faculty->name}}</option>
@endforeach
</select>



<label  class="label  is-size-4" for="surnames">Foto:</label>

<img width="200px" src="{{asset('storage').'/'. $program->photo}}" alt="Foto" class="my-4 mx-4">
<input  class="input" type="file" name="photo" value="photo" >


<input class="button has-background-success has-text-white my-4 mt-4" type="submit" value="Actualizar Datos del Programa">

</form>


@endforeach
@endsection


