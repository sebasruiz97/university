@extends('layouts.administrator.app')

@section('content')

<h2 class="has-text-info has-text-centered is-size-3 mb-4 mt-4"> Registrar Programa</h2>

@if(count($errors)>0)
<div class="notification is-danger ">
  <button class="delete"></button>
  <ul>
  @foreach($errors->all() as $error)
<li>{{$error}}</li>
  @endforeach
  </ul>
 </div>
 @endif

<form class='mb-4' action="{{route('program.store')}}" method="post" enctype="multipart/form-data">

@csrf




  <div class="form-control my-4 ">
<label class=" is-size-4 my-4" for="names"> Escriba el Programa: </label>
<input class="ml-3 my-4 input {{$errors->has('name')?'is-danger':''}}  "  type="text" name="name" placeholder="Escriba programa:">
{!! $errors->first('name', '<p class="help is-danger">:message</p>') !!}
  </div>


  <div class="form-control my-4">
<label for="city_residence_id" class=" is-size-4 my-4"> Selecciona la Facultad: </label>
<select class="select ml-3 my-4 input {{$errors->has('faculty_id')?'is-danger':''}}  " name="faculty_id" >
@foreach($faculties as $faculty)
<option  value="{{$faculty->id}}">{{$faculty->name}}</option>
@endforeach
</select>
{!! $errors->first('faculty_id', '<p class="help is-danger">:message</p>') !!}
</div>



  <div class="form-control my-4">
<label for="surnames" class=" is-size-4 my-4"> Inserte Foto del Programa: </label>
<input  class="ml-3 my-4 input {{$errors->has('name')?'is-danger':''}} " type="file" name="photo" placeholder="Foto del programa:" >
{!! $errors->first('photo', '<p class="help is-danger">:message</p>') !!}
</div>


<a class="button has-background-grey has-text-white" href="{{route('program.index')}}">Volver</a>
<input type="submit" class="button has-background-success has-text-white" value="Registrar Programa">




</form>





@endsection



